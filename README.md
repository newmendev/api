# Krakend Gateway для SVOE

### 1. Переименовать все файлы в папке config/partials: убрать расширение .example

### 2. Добавить переменные окружения для корректной работы
- FC_ENABLE - всегда 1
- FC_SETTINGS - путь до папки config/settings
- FC_PARTIALS - путь до папки config/partials
- FC_TEMPLATES - путь до папки config/templates
- FC_OUT - путь для автогенерируемого файла конфига